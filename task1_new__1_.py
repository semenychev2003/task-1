# Объявление стартовых значений
func = 42
last_var = None


# Чтение файла и вычисление результата
def calc(func):
    global last_var
    while True:
        try:
            data_var = int(input())
            func = func + 1 / data_var
        except (EOFError, ValueError):
            if last_var == data_var:
                break
            else:
                print('Ошибка в вычислении, последние введенные данные - ', data_var)
                last_var = data_var
                continue
    print('Сумма ряда: ', func)


# Вывод результата
calc(func)
